//---------------------------------------------------------------------------

#ifndef FSI_PowerOFFH
#define FSI_PowerOFFH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Samples.Gauges.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.WinXCtrls.hpp>
#include <Vcl.Menus.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *Button_5min_;
	TTimer *Timer1;
	TStatusBar *StatusBar1;
	TButton *Button_15min_;
	TButton *Button_30min_;
	TButton *Button_45min_;
	TButton *Button_1h_;
	TButton *Button_1h_30min_;
	TButton *Button_1h_45min_;
	TButton *Button_2h_;
	TButton *Button_2h_30min_;
	TButton *Button_3h_;
	TButton *Button_3h_30min_;
	TButton *Button_4h;
	TPopupMenu *PopupMenu1;
	TMenuItem *ExitPopupMenu_;
	void __fastcall Button_5min_Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Button_15min_Click(TObject *Sender);
	void __fastcall Button_30min_Click(TObject *Sender);
	void __fastcall Button_45min_Click(TObject *Sender);
	void __fastcall Button_1h_Click(TObject *Sender);
	void __fastcall Button_1h_30min_Click(TObject *Sender);
	void __fastcall Button_1h_45min_Click(TObject *Sender);
	void __fastcall Button_2h_Click(TObject *Sender);
	void __fastcall Button_3h_Click(TObject *Sender);
	void __fastcall Button_2h_30min_Click(TObject *Sender);
	void __fastcall Button_3h_30min_Click(TObject *Sender);
	void __fastcall Button_4hClick(TObject *Sender);
	void __fastcall ExitPopupMenu_Click(TObject *Sender);
private:	// User declarations
    void __fastcall RunTimer();
	AnsiString GetTimerStatusString(int sec);

public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
