//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FSI_PowerOFF.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
static int secOff = 0;
time_t startTimePoint;

__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	StatusBar1->Panels->Items[0]->Text = "";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RunTimer() {
	startTimePoint = time(NULL);
	Timer1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	time_t currTime = time(NULL);
	int curSec = difftime(currTime, startTimePoint);
	if (curSec >= secOff)
	{
		Timer1->Enabled = false;
		WinExec("shutdown -s -t 0", SW_HIDE);
	}
	else
	{
		StatusBar1->Panels->Items[0]->Text = GetTimerStatusString(secOff - curSec);
	}
}
//---------------------------------------------------------------------------

AnsiString TForm1::GetTimerStatusString(int sec) {
	int min = sec / 60;
	int hour = (sec / 3600);
	String status = "����� �� ���������� �����: ";
	if(sec <= 60)
		status += IntToStr(sec) + " ���. ";
	else if(sec > 60 && sec < 3600) {
		sec = sec - min * 60;
		status += IntToStr(min) + " ���. " + IntToStr(sec) + " ���. ";
	}
	else if(sec > 3600) {
		sec = sec - min * 60;
		min = min - hour * 60;
		status += IntToStr(hour) + " ���. " + IntToStr(min) + " ���. " + IntToStr(sec) + " ���. ";
	}
	return status;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_5min_Click(TObject *Sender)
{
	secOff = 60*5;
    RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_15min_Click(TObject *Sender)
{
	secOff = 60*15;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_30min_Click(TObject *Sender)
{
	secOff = 60*30;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_45min_Click(TObject *Sender)
{
	secOff = 60*45;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_1h_Click(TObject *Sender)
{
	secOff = 60*60;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_1h_30min_Click(TObject *Sender)
{
	secOff = 60*90;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_1h_45min_Click(TObject *Sender)
{
	secOff = 60*105;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_2h_Click(TObject *Sender)
{
	secOff = 60*120;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_2h_30min_Click(TObject *Sender)
{
	secOff = 60*150;
	RunTimer();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button_3h_Click(TObject *Sender)
{
	secOff = 60*180;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_3h_30min_Click(TObject *Sender)
{
	secOff = 60*210;
	RunTimer();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button_4hClick(TObject *Sender)
{
	secOff = 60*240;
	RunTimer();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::ExitPopupMenu_Click(TObject *Sender)
{
	Timer1->Enabled = false;
    Close();
}
//---------------------------------------------------------------------------


